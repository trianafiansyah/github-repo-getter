import flask
import json

from flask import request
from web_scraping import get_repositories
app = flask.Flask(__name__)
app.config["DEBUG"] = True


@app.route('/get_repository', methods=['GET'])
def home():
    username = request.args.get('username', None)
    if username is None:
        return flask.render_template('error.html'), 404
    else:
        data = get_repositories(username)
        return {
            'data':data,
            'success': 'true',
            'status': 200,
        }


app.run()