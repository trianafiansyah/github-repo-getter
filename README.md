# REPO

Get List Repositories From Github

## Installation

Make sure python is installed and use [pip](https://pip.pypa.io/en/stable/) to install dependencies.

```bash
pip install requirement.txt
```

## Usage

```python
python app.py
```

## See Logs For Port

http://localhost:<port>/get_repository?username=<username>

