import json

from selenium import webdriver 
from selenium.webdriver.common.by import By 
from selenium.webdriver.support.ui import WebDriverWait 
from selenium.webdriver.support import expected_conditions as EC 
from selenium.common.exceptions import TimeoutException


def get_repositories(user):
    option = webdriver.ChromeOptions()
    option.add_argument(" — incognito")

    browser = webdriver.Chrome(executable_path="./chromedriver.exe", chrome_options=option)

    browser.get("https://github.com/"+user+"?tab=repositories")

    timeout = 20
    try:
        WebDriverWait(browser, timeout).until(EC.visibility_of_element_located((By.XPATH, "//img[@class='avatar width-full height-full rounded-2']")))
    
        # find_elements_by_xpath returns an array of selenium objects.
        titles_element = browser.find_elements_by_xpath("//a[@itemprop='name codeRepository']")
        titles_link_element = browser.find_elements_by_xpath("//a[@itemprop='name codeRepository']")

        # use list comprehension to get the actual repo titles and not the selenium objects.
        titles = [x.text for x in titles_element]
        links = [x.get_attribute('href') for x in titles_link_element]
        # print out all the titles.

        description_tag = browser.find_elements_by_xpath("//p[@itemprop='description']")
        # same concept as for list-comprehension above.
        description = [x.text for x in description_tag]

        

        language_element = browser.find_elements_by_xpath("//span[@itemprop='programmingLanguage']")
        # same concept as for list-comprehension above.
        languages = [x.text for x in language_element]

        results = []
        for title, language, description, links  in zip(titles, languages, description, links):
            # print("RepoName : Language : description")
            # print(title + ": " + language+ ": " + description , '\n')
            results.append({"title": title, "language": language, "description": description, 'links': links})

        browser.quit()
        return results
    except TimeoutException:
        print('Timeout')
        browser.quit()